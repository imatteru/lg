$(function(){

    // full page functionality
	$('#fullpage').fullpage({
        anchors: ['durable', 'energy-saving', 'fast-cooling', 'low-noise', 'precise-temperature-control', 'design', 'more',/* 'reviews',*/ 'where-to-buy',],
        // fadingEffect:true,
        normalScrollElements: '.where__panel, .mobile-menu, .overlay, .modal',
        onLeave: function(index, nextIndex, direction) {
        	$('.header__nav a, .mobile-menu__list a').removeClass('active');
            $('.header__nav a[data-section-id='+nextIndex+'], .mobile-menu__list a[data-section-id='+nextIndex+']').addClass('active');
		}
	});

	// carousel for first section
	var owl = $('.owl-carousel').owlCarousel({
		items: 1,
        mouseDrag: false,
        touchDrag: false
    });

	$('.mobile-menu__list a').on('click', function(){
	   $('.mobile-menu, .header__burger').toggleClass('active');
    });

	// animations for first section
    owl.on('translate.owl.carousel', function(e) {
        $('.owl-item .container').addClass('animate');
        $('.owl-item.active .container').removeClass('animate');
    });

    // change slides with buttons
    $('.section-01__next').click(function() {
        owl.trigger('next.owl.carousel', [800]);
        $(this).removeClass('active');
        $('.section-01__prev').addClass('active');
    });

    $('.section-01__prev').click(function() {
        owl.trigger('prev.owl.carousel', [800]);
        $(this).removeClass('active');
        $('.section-01__next').addClass('active');
    });

    // prevent scrolling with modals
    $( '.overlay, .modal' ).on( 'mousewheel DOMMouseScroll', function ( e ) {
        var e0 = e.originalEvent,
            delta = e0.wheelDelta || -e0.detail;

        this.scrollTop += ( delta < 0 ? 1 : -1 ) * 10;
        e.preventDefault();
    });

    // closing modals
    $('.overlay, .js-close-modal').on('click', function(e){
        e.preventDefault();
        $('.modal, .overlay').removeClass('active');
    });

    // open modal for selecting location
    $('.js-open-modal-choose').on('click', function(e){
        e.preventDefault();
        $('.overlay, .modal-choose').addClass('active');
    });

    $('.js-change-location').on('click',function(e){
        e.preventDefault();
        if ( !$(this).hasClass('active') ) {
            var newLocation = $(this).data('location');
            $('.modal-choose__el').removeClass('active');
            $(this).addClass('active');
            $('.overlay, .modal-choose').removeClass('active');
            $('.header__location-current').data('location',newLocation).html(newLocation);
        }
    });

    //
    $('.js-no-change-location').on('click', function(e){
       $('.modal-quest, .modal-choose').toggleClass('active');
    });

    $('.modal-quest__yes').on('click', function(e){
       $('.overlay, .modal-quest').removeClass('active');
    });

    // custom scrollbar for reviews
    $(".reviews-list").mCustomScrollbar({
        axis:"x" // horizontal scrollbar
    });

    $('.js-open-mobile-menu').on('click', function(e){
        e.preventDefault();
        $(this).toggleClass('active');
        $('.header, .mobile-menu').toggleClass('active');
    });

    $('.popup-youtube, .popup-vimeo, .popup-gmaps, .js-open-fullscreen').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,

        fixedContentPos: false
    });

    // show / hide map for mobiles
    $('.where__show-map').on('click', function(){
       $('.where__panel').addClass('hidden');
       $('.where__show-map, .where__hide-map').toggleClass('active');
    });

    $('.where__hide-map').on('click', function(){
        $('.where__panel').removeClass('hidden');
        $('.where__show-map, .where__hide-map').toggleClass('active');
    });

});